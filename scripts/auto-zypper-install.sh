#!/usr/bin/env expect
# This script uses zypper to install and auto selects 1.
if [ $# -eq 0 ]
  then
    echo "Usage: ./auto-zypper-install.sh <name of package you wanna install"
fi
spawn zypper install $1
expect {
  "Choose from above solutions *" {
    send -- "1\r"
    exp_continue
  }

}

#expect eof
