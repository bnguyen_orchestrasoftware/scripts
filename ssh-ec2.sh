#!/bin/bash
#This is the IT aws pem file.
if [ $# -eq 0 ]
  then
    echo "Usage: ssh-bnguyen <aws-username> <IP address>"
    exit
fi

echo "Running command ssh -i ~/.aws/$1.pem ec2-user@$2"
ssh -i ~/.aws/$1.pem ec2-user@$2
